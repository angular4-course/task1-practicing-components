import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-success-alert',
  template:  require('pug-loader!./success-alert.component.pug')(),
  styleUrls: ['./success-alert.component.css']
})
export class SuccessAlertComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
