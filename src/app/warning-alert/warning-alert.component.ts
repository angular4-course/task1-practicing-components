import { Component } from '@angular/core';

@Component({
  selector: 'app-warning-alert',
  template:  require('pug-loader!./warning-alert.component.pug')(),
  styleUrls: ['./warning-alert.component.css']
})
export class WarningAlertComponent {

}
